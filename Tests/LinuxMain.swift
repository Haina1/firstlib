import XCTest

import FirstLibTests

var tests = [XCTestCaseEntry]()
tests += FirstLibTests.allTests()
XCTMain(tests)
