public struct MyLib {
    var text = "Hello, MyLib!"
    public var num:Int
    public init() {
        num = 2
    }
}

import SwiftyJSON

//SwiftyJSON
let json = JSON(["name":"Jack", "age": 25])
print(json)

//自定义库
let lib = MyLib()
print(lib)
